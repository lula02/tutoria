package mendozasantana.facci.taller.Adapter;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.ArrayList;

import mendozasantana.facci.taller.Modelo.Usuarios;
import mendozasantana.facci.taller.R;


    //link de referencia ->https://developer.android.com/guide/topics/ui/layout/recyclerview#java

    //clase para manejar o llenar la RecyclerView (ListView pero mejor organizada)
    //se Extiende a recyclerView y  el metodo Adapter la cual recibe la misma clase y una clase que se escribe dentro de la misma clase
    //que es la llamada ViewHolder (Le pueden cambiar el nombre si es que desean
public class AdaptdorReciclerView extends RecyclerView.Adapter<AdaptdorReciclerView.ViewHolder>{

    //Instancia de tipo ArrayList, la cual hace uso de la clase Usuario, que se encuentra el el packet Modelo
    private ArrayList<Usuarios> usuariosL;
    //instancia ProgressDialog que ya saben que es
    private ProgressDialog progressDialog;
    //instancia de la URL del web_service
    private String URL_USER = "http://192.168.43.72:90/comunidad_service/usuarios/";
    //constructor de la clase, recibe, un parametro de tipo ArrayList
    public AdaptdorReciclerView(ArrayList<Usuarios> usuariosL) {
        this.usuariosL = usuariosL;
    }

    @NonNull
    @Override
    //metododo Oncreate sirve para darle vista a cada item del ReciclerView
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_recycler_view,parent,false);
        return new ViewHolder(view);
    }

    @Override
    //metodo donde se setean o escribe en los campos de cada item, el cual tiene dos parametros uno pertenece a la clase
    //viewHolder y el otro a el adaptador y nos permitira optener la posicion del item del recyclerView
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        //se crea un objeto de la clase Usuarios (Del packet Modelo), la cuall sera igual a la instancia creada a nivel de clase
        //que es de tipo ArrayList y se usa el metodo get y le pasamos el parametro posicion
        final Usuarios usuarios = usuariosL.get(position);
        //se escribe en cada view del item, para acceder a ellos se hace uso del parametro de la clase viewHolder
        holder.nombres.setText(usuarios.getNombres());
        holder.apellidos.setText(usuarios.getApellidos());

        //aqui obtenemos el View al que le vamos a hacer el click
        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Context context = view.getContext();
                //creamos un AlertView, de la manera mas sencilla y rapida (hay varias y muchas personalizadas)
                AlertDialog.Builder dialogo1 = new AlertDialog.Builder(context);
                dialogo1.setTitle("Cuidado..!");
                dialogo1.setMessage("¿Realmente Quiere Eliminar El Usuario?");
                dialogo1.setCancelable(false);
                dialogo1.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogo1, int id) {
                        //aqui se obtiene el ID
                        String id_usuario = usuarios.getId();
                        progressDialog = new ProgressDialog(context);
                        progressDialog.setMessage("ELIMINANDO USUARIO");
                        progressDialog.setCancelable(false);
                        progressDialog.show();
                        //esto es lo mismo que el login o el registro
                        //la unica diferencia es que es de metodo DELETE y la url se le concatena el ID

                        StringRequest stringRequest = new StringRequest(Request.Method.DELETE, URL_USER+id_usuario, new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                //preguntamos que si lo que responde el servicio es igual a 1
                                if (response.equals("1")){
                                    progressDialog.dismiss();
                                    //al ArrayList le eliminamos un iten, usando la posicion del iten que seleccionamos
                                    usuariosL.remove(position);
                                    //y notificamos al RecyclerView que se elimino un campo
                                    notifyItemRemoved(position);
                                }else {
                                    progressDialog.dismiss();
                                }
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                progressDialog.dismiss();
                                Toast.makeText(context, "ERROR: " + error.toString() +
                                        " EN LA BASE DE DATOS Y O SERVIDOR..!", Toast.LENGTH_SHORT).show();
                            }
                        });
                        RequestQueue requestQueue= Volley.newRequestQueue(context);
                        requestQueue.add(stringRequest);
                    }
                });
                dialogo1.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogo1, int id) {
                        dialogo1.dismiss();
                    }
                });
                dialogo1.show();

            }
        });
    }

    @Override
    public int getItemCount() {
        //este método nos permite contar los item del recyclerView
        return usuariosL.size();
    }

    //esta es la clase que se crea dentro de otra clase, la cual mencioné al principio
    //si se fijan extiende de un RecyclerView
    public class ViewHolder extends RecyclerView.ViewHolder {
        //instancias de los View creados en el layout que se llama en el onCreate
        private TextView nombres, apellidos;
        public ImageView imageView;

        //constructor de la clase
        @SuppressLint("WrongViewCast")
        public ViewHolder(View itemView) {
            super(itemView);
            //inicializamos los view
            nombres = (TextView) itemView.findViewById(R.id.LBLNombres);
            apellidos = (TextView) itemView.findViewById(R.id.LBLApellidos);
            //id = (TextView) itemView.findViewById(R.id.LBLID);
            imageView = (ImageView) itemView.findViewById(R.id.imgDelete);
        }
    }

}
