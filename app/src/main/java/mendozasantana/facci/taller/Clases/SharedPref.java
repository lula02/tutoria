package mendozasantana.facci.taller.Clases;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import java.util.HashMap;

public class SharedPref {

    //nombre de shared prefef
    public static final String SHARED_PREF_NAME = "LOGIN";

    //Username
    public static final String USER_ID = "id";
    public static SharedPref mInstance;
    public static Context mCtx;


    //constructor, recibe como parametro un Context
    public SharedPref(Context context) {
        mCtx = context;
    }


    public static synchronized SharedPref getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new SharedPref(context);
        }
        return mInstance;
    }


    //method to store user data
    public void storeUserName(String id) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(USER_ID, id);
        editor.commit();
    }

    //check if user is logged in
    public boolean isLoggedIn() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(USER_ID, null) != null;
    }


    //find logged in user
    //con esto podremos obtener los datos aue guardamos, y asi poder hacer uso de ellos donde los necesitemos
    //por ejemplo estamos guardando el ID
    //si llegaramos a necesitar el ID para hacer una consulta de ese usuario, una manera facil de obtenerlo es desde aqui
    public HashMap<String, String> LoggedInUser() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        HashMap<String, String> usuario = new HashMap<>();
        usuario.put(USER_ID, sharedPreferences.getString(USER_ID, null));
        return usuario;

    }

}
