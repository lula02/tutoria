package mendozasantana.facci.taller.Actividades;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import mendozasantana.facci.taller.Adapter.AdaptdorReciclerView;
import mendozasantana.facci.taller.Modelo.Usuarios;
import mendozasantana.facci.taller.R;

import static mendozasantana.facci.taller.Clases.SharedPref.SHARED_PREF_NAME;
import static mendozasantana.facci.taller.Clases.SharedPref.mCtx;

public class Home extends AppCompatActivity {

    //esto ya deberian saber que son las instancias dx...
    private RecyclerView recyclerView;
    private AdaptdorReciclerView adaptdorReciclerView;
    private RecyclerView.LayoutManager layoutManager;
    private ArrayList<Usuarios> usuariosArrayList;
    private String URL_USUARIOS = "http://192.168.43.72:90/comunidad_service/usuarios";
    private ProgressDialog progressDialog;
    private Button cerrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        //aqui se inicializan
        recyclerView = (RecyclerView)findViewById(R.id.RCV);
        usuariosArrayList=new ArrayList<>();
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        adaptdorReciclerView = new AdaptdorReciclerView(usuariosArrayList);
        progressDialog = new ProgressDialog(this);
        cerrar = (Button)findViewById(R.id.BTNCerrar);
        //onclick del boton cerrar secion
        cerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //esto sirve para eliminar el SharedPref
                SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.clear();
                editor.commit();
                //finalizar actividad
                finish();
                //lanzar una nueva
                startActivity(new Intent(Home.this, Login.class));
            }
        });
        User();
    }

    private void User() {

        progressDialog.show();
        //el mismo cuento del login, registro y eliminar
        //lo que vuelve a cambiar es el metodo que ya no es ni post ni delete, es GET
        StringRequest stringRequest=new StringRequest(Request.Method.GET, URL_USUARIOS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    progressDialog.dismiss();
                    //esto es un JsonObject y se obtiene desde la repuesta
                    JSONObject jsonObject = new JSONObject(response);
                    //esto es nuevo, y es un JSONArray el cual va a ser igual al JsonObject que esta antes de esto
                    //y a la vez va a necesitar un nombre que es el ue le damos en el web_service
                    //en este caso es "usuarios"
                    JSONArray array=jsonObject.getJSONArray("usuarios");
                    //se crea un for, el cual e va a ejecutar mientras en entero i sea menos al array creado anteriormente
                    for (int i=0; i<array.length(); i++ ){
                        //creamos un nuevo JsonObject y este va hacer igual al array pero obteniendo un JSONObject mediante
                        //el entero i que va a imprimir el for en cada vuelta
                        JSONObject jsonObject1 = array.getJSONObject(i);
                        //creamos un objeto de la clase USUARIOS (de packet modelo)
                        Usuarios usuarios = new Usuarios();
                        //hacemos uso de ese objeto y seteamos cada campo, obviamente haciendo uso tambien del
                        //jsonObject y obteniendo un String o un Entero dependiendo lo que nos responda el server
                        //y cuando es entero se necesita hacer la conversion a String (String.valueOf) ya que en la clase
                        //USUARIO hay creadas instancias de tipo String
                        usuarios.setNombres("NOMBRES: "+jsonObject1.getString("nombres"));
                        usuarios.setApellidos("APELLIDOS: "+jsonObject1.getString("apellidos"));
                        usuarios.setId(String.valueOf(jsonObject1.getInt("id")));
                        //al ArrayList que creamos a nivel de clase se le agrega el objeto de tipo usuario
                        usuariosArrayList.add(usuarios);
                    }
                    //aquí es donde se va creando cada item del recyclerView (ListView mas ordenada)
                    recyclerView.setAdapter(adaptdorReciclerView);
                } catch (JSONException e) {
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("LOG", error.toString());
            }
        });
        RequestQueue requestQueue= Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}

