package mendozasantana.facci.taller.Actividades;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import mendozasantana.facci.taller.Clases.SharedPref;
import mendozasantana.facci.taller.R;

//aqui implementamos en metodo onclick, el cual nos pide sobreescribir un metodo
public class Login extends AppCompatActivity implements View.OnClickListener{

    //instancias de los View e instancias de tipo String para la url
    private EditText correo, clave;
    private Button login, register;
    private static final String URL_LOGIN= "http://192.168.43.72:90/comunidad_service/login";
    //instancias de la clase SharedPref la cual nos va a servir para comprobar si un usuario esta logeado o no
    //tambien nos servira para crear una sesion del Usuario
    private SharedPref sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //inicializamos en sharedPref
        sharedPref = new SharedPref(this);
        //preguntamos si existe algun dato
        //llamando al metodo isLoggedIn, que se encuentra en la clase SharedPref
        if (sharedPref.isLoggedIn()){
            //si es asi lanzamos una nueva actividad
            startActivity(new Intent(Login.this, Home.class));
            finish();
        }else {
            //de no serlo, inicializamos los View
            correo = (EditText) findViewById(R.id.TXTCorreoL);
            clave = (EditText) findViewById(R.id.TXTClaveL);
            login = (Button) findViewById(R.id.BTNLoginL);
            register = (Button) findViewById(R.id.BTNRegistrarL);
            login.setOnClickListener(this);
            register.setOnClickListener(this);
        }

    }

    @Override
    //este es el metodo que se tiene que sobreescribir
    public void onClick(View view) {
        //creamos un switch-case el cual se va a buscar un View mediante su ID
        switch (view.getId()){
            //en cada CASE ponemos el ID del View que le vallamos a hacer CLick
            case R.id.BTNLoginL:

                //preguntamos si los View de tipo EditText se encuentran vacios
                if (correo.getText().toString().isEmpty()){
                    correo.setError("CAMPO NECESARIO");
                }else if (clave.getText().toString().isEmpty()){
                    clave.setError("CAMPO NECESARIO");
                    //si no estan vacios se ejecuta el metodo ISesion
                }else {
                    ISesion();
                }

                break;

            case R.id.BTNRegistrarL:
                startActivity(new Intent(this, Registro.class));
                finish();
                break;
        }
    }

    private void ISesion() {
        //crear progress
        final ProgressDialog progressDialog = new ProgressDialog(this);
        //para que no se cancele si se presiona en la pantalla
        progressDialog.setCancelable(false);
        //mensaje que se muestra
        progressDialog.setMessage("CONSULTANDO..!");
        //mostrar el progress
        progressDialog.show();

        //creamos una instancia de tipo StringRequest (si no implementa la libreria Volley no les va a salir esta Instancia)
        //luego la inicializamos y nos va a pedir que le digamos cuatro cosas:
        //1 el tipo de metodo que vamos a utilizar en este caso es POST
        //2 una URL a donde apuntar (nos la da el web_service)
        //3 un nuevo metodo el cual nos dira si_todo esta bien
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_LOGIN, new Response.Listener<String>() {
            @Override
            //aqui llega lo que nos responde el servidor o lo que le hemos configurado que responda
            //llega en ese parametro de tipo String
            public void onResponse(String response) {
                //preguntamos si la repuesta es igual a algo que nosotros configuramos en el server
                //y asi vamos haciendo lo necesario

                //ejemplo 404 es por que no existe un usuario, entonces una forma facil y sencilla de decirle
                //al usuario o indicarle que escribio mal el correo o que aun no esta registrado
                //es mediante un Toast
                if (response.equals("404")){
                    //disminuir el progress
                    progressDialog.dismiss();
                    Toast.makeText(Login.this, "EL USUARIO NO EXISTE", Toast.LENGTH_SHORT).show();
                }else if (response.equals("10")){
                    progressDialog.dismiss();
                    Toast.makeText(Login.this, "USUARIO O CLAVE INCORRECTA", Toast.LENGTH_SHORT).show();
                }else {
                    progressDialog.dismiss();
                    //en caso de que este correcto
                    //nececitaremos un try catch
                    try {
                        //creamos un Jsonbject, el cual es igual a la repuesta ya que configuramos
                        //que nos devuelva un jsonObject
                         JSONObject jsonObject = new JSONObject(response);
                        Log.e("111", response);
                        //luego obtenemos un dato de ese jsonObject
                        Integer id = (Integer) jsonObject.get("id");
                        //aqui volvemos a hacer uso de la instancia de tipo SharedPref
                        //y llamamos al metodo de crear User
                        sharedPref.getInstance(getApplicationContext()).storeUserName(id.toString());
                        //finalizamos la actividad actual
                        finish();
                        //lanzamos la nueva
                        startActivity(new Intent(Login.this, Home.class));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            //4 creamos un nuevo metodo que va hacer en caso de que el server responda algun error
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ERROR", error.toString());
                Toast.makeText(Login.this, error.toString(), Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        }){
            //metodo getParams, el cúal sirve para enviar o agregar los datos a la instancia creada anteriormente
            //que era de tipo StringRequest y pertenece a la libreria volleu
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("idn", correo.getText().toString().trim());
                map.put("password", clave.getText().toString().trim());
                return map;
            }
        };
        //Este objeto se encarga de gestionar automáticamente el envió de las peticiones,
        //la administración de los hilos, la creación de la caché y la publicación de resultados
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(stringRequest);
    }
}
