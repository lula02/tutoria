package mendozasantana.facci.taller.Actividades;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

import mendozasantana.facci.taller.R;

public class Registro extends AppCompatActivity implements View.OnClickListener{

    //esto no es necesario comentar ya que basicament es lo mismo que el login
    private EditText nombre, apellido, correo, clave;
    private Button registar, login;
    private static final String URL_REGISTRAR= "http://192.168.43.72:90/comunidad_service/usuarios";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        nombre = (EditText) findViewById(R.id.TXTNombreR);
        apellido = (EditText) findViewById(R.id.TXTApellidoR);
        correo = (EditText) findViewById(R.id.TXTCorreoR);
        clave = (EditText) findViewById(R.id.TXTClaveR);

        registar = (Button) findViewById(R.id.BTNRegistrarR);
        login = (Button) findViewById(R.id.BTNLoginR);
        registar.setOnClickListener(this);
        login.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.BTNRegistrarR:

                if (nombre.getText().toString().isEmpty()){
                    nombre.setError("NECESARIO");
                }else if (apellido.getText().toString().isEmpty()){
                    apellido.setError("NECESARIO");
                }else if (correo.getText().toString().isEmpty()){
                    correo.setError("NECESARIO");
                }else if (clave.getText().toString().isEmpty()){
                    clave.setError("NECESARIO");
                }else {

                    Registrar();

                }
                break;

            case R.id.BTNLoginR:
                startActivity(new Intent(this, Login.class));
                finish();
                break;

        }
    }

    private void Registrar() {

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("REGISTRANDO USUARIO");
        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_REGISTRAR, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response.equals("1")){
                    Toast.makeText(Registro.this, "EXITO", Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                    startActivity(new Intent(Registro.this, Login.class));
                    finish();
                }else {
                    Toast.makeText(Registro.this, "ERROR", Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ERROR", error.toString());
                Toast.makeText(Registro.this, error.toString(), Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();

                map.put("nombres", nombre.getText().toString().trim());
                map.put("apellidos", apellido.getText().toString().trim());
                map.put("idn", correo.getText().toString().trim());
                map.put("password", clave.getText().toString().trim());
                return map;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(stringRequest);
    }
}
